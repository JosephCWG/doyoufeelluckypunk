//
//  FirstViewController.swift
//  DoYouFeelLuckyPunk
//
//  Created by Watts,Joseph C on 2/21/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import UIKit

class GuesserViewController: UIViewController {
    @IBOutlet weak var guessTextField: UITextField!
    @IBOutlet weak var feedbackLabel: UILabel!
    @IBOutlet weak var amIRightButton: UIButton!
    
    let defaultFeedback = "Enter your guess between 1 and 10"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        feedbackLabel.text = defaultFeedback // empty the text field on load.
        Guesser.shared.startNewRound()
    }

    // Send a guess to the guesser, set the text and disable the guess button
    @IBAction func amIRightButtonPressed(_ sender: Any) {
        if let guess = Int(guessTextField.text!) {
            // validate user input a second time
            if guess > 10 || guess < 1 {
                displayInvalidGuess()
                return
            }
            
            // if the result was the correct answer congratulate the user
            let result = Guesser.shared.makeAGuess(guess)
            if result == Guesser.answerFeedback.exact {
                // display the congrats window
                Guesser.shared.addCurrentGameToHistory()
                amIRightButton.isEnabled = false
                // minimize the keyboard and display congrats
                self.view.endEditing(true)
                displayAlert(title: "Well done", message: "You guessed correctly in \(Guesser.shared.currentNumberOfAttempts) tries! Press the 'Create New Problem' button to continue playing!")
            }
            feedbackLabel.text = result.rawValue
        } else {
            displayInvalidGuess()
        }
    }
    
    func displayInvalidGuess() {
        displayAlert(title: "Invalid Guess", message: "Please Enter A Valid Guess Between the values of 1 & 10")
    }
    
    func displayAlert(title:String, message:String) {
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // Generate a new round, clear the tf/labels, and unlock the amIRight button
    @IBAction func createNewProblemButtonPressed(_ sender: Any) {
        Guesser.shared.startNewRound()
        amIRightButton.isEnabled = true
        feedbackLabel.text = defaultFeedback
        guessTextField.text = ""
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
