//
//  StatisticsViewController.swift
//  DoYouFeelLuckyPunk
//
//  Created by Watts,Joseph C on 2/21/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import UIKit

class StatisticsViewController: UIViewController {

    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var maxLabel: UILabel!
    @IBOutlet weak var meanLabel: UILabel!
    @IBOutlet weak var stdDevLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // Any time the tab is clicked on
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshLabels()
    }
    
    @IBAction func clearStatisticsButtonPressed(_ sender: Any) {
        Guesser.shared.clearPreviousGames()
        refreshLabels()
    }
    
    func refreshLabels() {
        let previousGuesses = Guesser.shared.previousGuesses
        if previousGuesses.count == 0 {
            minLabel.text = "0"
            maxLabel.text = "0"
            meanLabel.text = "0"
            stdDevLabel.text = "0"
        } else {
            var min = previousGuesses[0].numberGuesses;
            var max = previousGuesses[0].numberGuesses;
            var mean:Double = 0
            var stdDev:Double = 0
            
            // Loop through all of the variables to get mean, min, and max.
            for guess in previousGuesses {
                if guess.numberGuesses < min {
                    min = guess.numberGuesses
                } else if (guess.numberGuesses > max) {
                    max = guess.numberGuesses
                }
                mean += Double(guess.numberGuesses)
            }
            mean = mean / Double(previousGuesses.count)
            stdDev = calculateStandardDeviationOfGuesses(guesses: previousGuesses, mean: mean)
            
            minLabel.text = "\(min)"
            maxLabel.text = "\(max)"
            meanLabel.text = String(format: "%.3f", mean)
            stdDevLabel.text = String(format: "%.3f", stdDev)
        }
    }
    
    private func calculateStandardDeviationOfGuesses(guesses previousGuesses:[Guess], mean: Double) -> Double {
        var meanDifferenceSquared:[Double] = []
        for guess in previousGuesses {
            meanDifferenceSquared.append((Double(guess.numberGuesses) - mean) * (Double(guess.numberGuesses) - mean))
        }
        
        var meanOfSquares:Double = 0
        for meanDifference in meanDifferenceSquared {
            meanOfSquares += meanDifference
        }
        meanOfSquares = meanOfSquares / Double(meanDifferenceSquared.count)
        return sqrt(meanOfSquares)
    }
}
