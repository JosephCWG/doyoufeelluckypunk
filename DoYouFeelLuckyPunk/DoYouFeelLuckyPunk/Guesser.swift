//
//  GuessHistorySingleton.swift
//  DoYouFeelLuckyPunk
//
//  Created by Watts,Joseph C on 2/21/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import Foundation

class Guesser {
    // keep an enum of possible responses to display in the label
    enum answerFeedback: String {
        case low = "Your guess is too low!"
        case high = "Your guess is too high!"
        case exact = "Your guess was exact!"
    }
    
    static let shared = Guesser()
    private var _currentCorrectAnswer: Int = 0
    private var _currentNumberOfAttempts: Int = 0
    var currentNumberOfAttempts: Int {
        get {
            return _currentNumberOfAttempts
        }
    }
    
    // keep track of history
    private var _previousGuesses: [Guess] = []
    var previousGuesses: [Guess] {
        get {
            return _previousGuesses
        }
    }
    
    // add the current game to the statistics/history tab
    func addCurrentGameToHistory() {
        _previousGuesses.append(Guess(correctAnswer: _currentCorrectAnswer, numberGuesses: _currentNumberOfAttempts))
    }
    
    // function to clear statistics
    func clearPreviousGames() {
        _previousGuesses.removeAll()
    }
    
    // Wipe out the current round, generate a new problem.
    func startNewRound() {
        _currentCorrectAnswer = Int.random(in: 1...10)
        _currentNumberOfAttempts = 0
    }
    
    // Function to return whether the guess was too high, low, or exact.
    func makeAGuess(_ guess:Int) -> answerFeedback {
        _currentNumberOfAttempts += 1
        if guess > _currentCorrectAnswer {
            return answerFeedback.high
        } else if guess < _currentCorrectAnswer {
            return answerFeedback.low
        } else {
            return answerFeedback.exact
        }
    }
    
    private init(){}
}
